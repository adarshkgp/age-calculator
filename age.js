const MIN_YEAR_SUPPORTED = 1800;
const CURRENT_DATE = new Date();
const MAX_YEAR_SUPPORTED = CURRENT_DATE.getFullYear();
const MAX_MONTH_VALID = 12;
const LEAP_YEAR_FEB_DAYS = 29;

const numDays = {
  "1": 31,
  "2": 28,
  "3": 31,
  "4": 30,
  "5": 31,
  "6": 30,
  "7": 31,
  "8": 31,
  "9": 30,
  "10": 31,
  "11": 30,
  "12": 31,
};

const numMonths = {
  1: "January",
  2: "February",
  3: "March",
  4: "April",
  5: "May",
  6: "June",
  7: "July",
  8: "August",
  9: "September",
  10: "October",
  11: "November",
  12: "December",
};

const alphabetCheck = (inputtxt) => {
  let letters = /^[A-Za-z]+$/;
  if (inputtxt.match(letters)) {
    return true;
  } else {
    return false;
  }
};

const getValue = (elemId) => {
  let elemValue = document.getElementById(elemId).value;
  if (elemValue === "") {
    showResult("please fill the " + elemId, "error");
    throw "EMPTY INPUT EXCEPTION";
  } else {
    return elemValue;
  }
};

const getNumberValue = (elemId) => {
  const elemValue = getValue(elemId);
  const result = parseInt(elemValue);

  if (Number.isNaN(result)) {
    showResult("Invalid input detected for" + elemId, "error");
    throw "SOMETHING WENT WRONG";
  } else {
    return result;
  }
};

const maxMonthValidForMaxYear = (year) => {
  if (year === MAX_YEAR_SUPPORTED) {
    return CURRENT_DATE.getMonth() + 1;
  } else {
    return MAX_MONTH_VALID;
  }
};

const maxDaysValidForMonths = (year, month) => {
  if (year === MAX_YEAR_SUPPORTED) {
    return CURRENT_DATE.getDate();
  } else if (year % 4 === 0 && month === 2) {
    return LEAP_YEAR_FEB_DAYS;
  } else {
    return numDays[month];
  }
};

const fillOptions = (minLimit, maxlimit, elemId) => {
  let select = document.getElementById(elemId);
  select.innerHTML = "";

  for (let i = minLimit; i <= maxlimit; i++) {
    let el = document.createElement("option");
    el.text = i;
    el.value = i;
    select.add(el);
  }
};

const fillYears = () => {
  fillOptions(MIN_YEAR_SUPPORTED, MAX_YEAR_SUPPORTED, "years");
  fillMonthsAndDays();
};

const fillDays = () => {
  const year = getNumberValue("years");
  const month = getNumberValue("months");
  const maxDaysLimit = maxDaysValidForMonths(year, month);
  fillOptions(1, maxDaysLimit, "days");
};

const fillMonths = () => {
  const select = document.getElementById("months");
  const year = getNumberValue("years");
  const maxMonthLimit = maxMonthValidForMaxYear(year);
  select.innerHTML = "";
  for (let i = 1; i <= maxMonthLimit; i++) {
    let el = document.createElement("option");
    el.text = numMonths[i];
    el.value = i;
    select.add(el);
  }
};

const fillMonthsAndDays = () => {
  fillMonths();
  fillDays();
};

const showResult = (htmlElement, elemId) => {
  let x = document.getElementById(elemId);
  x.innerHTML += htmlElement;
};

const clearResult = (elemId) =>
  (document.getElementById(elemId).innerHTML = "");

const calculateAge = () => {
  clearResult("error");
  clearResult("result");
  const name = getValue("user-name");
  const year = getNumberValue("years");
  const month = getNumberValue("months");
  const day = getNumberValue("days");

  if (!alphabetCheck(name)) {
    showResult("please fill the name in  proper format!", "error");
  } else if (year < MIN_YEAR_SUPPORTED && year > MAX_YEAR_SUPPORTED) {
    showResult("please fill the years correlctly!", "error");
  } else if (month < 1 && month > maxMonthValidForMaxYear(year)) {
    showResult("please fill the months correlctly!", "error");
  } else if (day < 1 && day > maxDaysValidForMonths(year, month)) {
    showResult("please fill the days correlctly!", "error");
  } else {
    let yearPassed = CURRENT_DATE.getFullYear() - year;
    let monthPassed = CURRENT_DATE.getMonth() + 1 - month;
    if (monthPassed < 0) {
      monthPassed = 12 + monthPassed;
      yearPassed = yearPassed - 1;
    }

    showResult(
      name + " is " + yearPassed + " Years and " + monthPassed + " months old.",
      "result"
    );
  }
};

fillYears();
